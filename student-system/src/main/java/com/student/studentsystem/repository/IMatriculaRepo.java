package com.student.studentsystem.repository;

import com.student.studentsystem.model.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula,Integer> {

}
