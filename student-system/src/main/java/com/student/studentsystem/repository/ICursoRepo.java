package com.student.studentsystem.repository;

import com.student.studentsystem.model.Curso;

import java.util.List;

public interface ICursoRepo extends IGenericRepo<Curso,Integer> {
    List<Curso> findAllByEstado(boolean estado);

    Curso findByIdCursoPk(Integer id);
}
