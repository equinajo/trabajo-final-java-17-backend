package com.student.studentsystem.repository;

import com.student.studentsystem.model.DetalleMatricula;
import com.student.studentsystem.model.Matricula;

public interface IDetalleMatriculaRepo extends IGenericRepo<DetalleMatricula,Integer> {

}
