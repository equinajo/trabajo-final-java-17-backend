package com.student.studentsystem.repository;

import com.student.studentsystem.model.Estudiante;

public interface IEstudianteRepo extends IGenericRepo<Estudiante,Integer> {

    Estudiante findByIdEstudiantePk(Integer id);
}
