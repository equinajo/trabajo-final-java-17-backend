package com.student.studentsystem.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity

public class Estudiante {
    @Id
    @SequenceGenerator(name = "SEQ_ESTUDIANTE", sequenceName = "SEQ_ESTUDIANTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ESTUDIANTE")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_estudiante_pk",nullable = false)
    private Integer idEstudiantePk;
    @Column(nullable = false,length = 50)
    private String nombres;
    @Column(nullable = false,length = 50)
    private String apellidos;
    @Column(nullable = false,length = 15)
    private String dni;
    @Column(nullable = false)
    private int edad;
}
