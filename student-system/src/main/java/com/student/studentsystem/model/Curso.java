package com.student.studentsystem.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Curso {
    @Id
    @SequenceGenerator(name = "SEQ_CURSO", sequenceName = "SEQ_CURSO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CURSO")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_curso_pk",nullable = false)
    private Integer idCursoPk;
    @Column(nullable = false,length = 50)
    private String nombre;
    @Column(nullable = false,length = 50)
    private String sigla;
    @Column(nullable = false)
    private boolean estado;

}
