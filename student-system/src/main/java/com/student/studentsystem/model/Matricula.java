package com.student.studentsystem.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Matricula {
    @Id
    @SequenceGenerator(name = "SEQ_MATRICULA", sequenceName = "SEQ_MATRICULA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MATRICULA")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_matricula_pk",nullable = false)
    private Integer idMatriculaPk;
    @Column(nullable = false,length = 50)
    private Date fechaMatricula;
    @Column(nullable = false)
    private boolean estado;
    @JoinColumn(name = "id_estudiante_fk", referencedColumnName = "id_estudiante_pk",nullable = false,unique = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private Estudiante estudiante;

    @OneToMany(mappedBy = "matricula", cascade = CascadeType.ALL)
    private List<DetalleMatricula> listaDetalleMatricula;

}
