package com.student.studentsystem.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class DetalleMatricula {
    @Id
    @SequenceGenerator(name = "SEQ_DETALLE_MATRICULA", sequenceName = "SEQ_DETALLE_MATRICULA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DETALLE_MATRICULA")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_matricula_pk",nullable = false)
    private Integer idDetalleMatriculaPk;
    @JoinColumn(name = "id_curso_fk", referencedColumnName = "id_curso_pk",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Curso curso;
    @Column(nullable = false,length = 50)
    private String aula;
    @JoinColumn(name = "id_matricula_fk", referencedColumnName = "id_matricula_pk",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Matricula matricula;

}
