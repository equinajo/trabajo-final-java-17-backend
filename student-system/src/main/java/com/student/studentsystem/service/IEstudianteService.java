package com.student.studentsystem.service;

import com.student.studentsystem.dto.EstudianteDTO;
import com.student.studentsystem.model.Estudiante;

import java.util.List;

public interface IEstudianteService extends ICRUD<Estudiante,Integer>{

    List<Estudiante> listEstudentsOrderByDescForAge();

    List<Estudiante> listCourseAndStudentRelationship();

}
