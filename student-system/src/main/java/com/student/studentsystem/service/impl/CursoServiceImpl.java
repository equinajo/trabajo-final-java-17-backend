package com.student.studentsystem.service.impl;

import com.student.studentsystem.model.Curso;
import com.student.studentsystem.model.DetalleMatricula;
import com.student.studentsystem.repository.ICursoRepo;
import com.student.studentsystem.repository.IGenericRepo;
import com.student.studentsystem.service.ICursoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CursoServiceImpl extends CRUDImpl<Curso,Integer> implements ICursoService {

    private final ICursoRepo repo;

    @Override
    protected IGenericRepo<Curso, Integer> getRepo() {
        return repo;
    }

    @Override
    public List<Curso> listCursoMatriculados() {
        return repo.findAllByEstado(true);
    }
}
