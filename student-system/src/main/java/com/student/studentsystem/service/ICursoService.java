package com.student.studentsystem.service;

import com.student.studentsystem.model.Curso;
import com.student.studentsystem.model.DetalleMatricula;

import java.util.List;

public interface ICursoService extends ICRUD<Curso,Integer>{
    List<Curso> listCursoMatriculados();
}
