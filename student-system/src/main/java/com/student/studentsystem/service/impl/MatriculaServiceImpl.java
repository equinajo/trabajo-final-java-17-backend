package com.student.studentsystem.service.impl;

import com.student.studentsystem.dto.DetalleMatriculaDTO;
import com.student.studentsystem.dto.MatriculaDTO;
import com.student.studentsystem.model.Curso;
import com.student.studentsystem.model.DetalleMatricula;
import com.student.studentsystem.model.Estudiante;
import com.student.studentsystem.model.Matricula;
import com.student.studentsystem.repository.*;
import com.student.studentsystem.service.IMatriculaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MatriculaServiceImpl extends CRUDImpl<Matricula,Integer> implements IMatriculaService {

    private final IMatriculaRepo repo;
    private final IEstudianteRepo estudianteRepo;
    private final ICursoRepo cursoRepo;
    private final IDetalleMatriculaRepo detalleMatriculaRepo;

    @Override
    protected IGenericRepo<Matricula, Integer> getRepo() {
        return repo;
    }

    public void saveMatricula(MatriculaDTO obj){
        Matricula matricula = new Matricula();
        matricula.setEstado(obj.isEstado());
        matricula.setFechaMatricula(obj.getFechaMatricula());
        Estudiante estudiante = estudianteRepo.findByIdEstudiantePk(obj.getIdEstudiante());
        matricula.setEstudiante(estudiante);
        matricula = repo.save(matricula);
        if(obj.getListaDetalleMatricula()!=null && !obj.getListaDetalleMatricula().isEmpty()){
            for(DetalleMatriculaDTO dm:obj.getListaDetalleMatricula()){
                DetalleMatricula detalleMatricula = new DetalleMatricula();
                detalleMatricula.setAula(dm.getAula());
                Curso curso = cursoRepo.findByIdCursoPk(dm.getIdCurso());
                detalleMatricula.setCurso(curso);
                detalleMatricula.setMatricula(matricula);
                detalleMatriculaRepo.save(detalleMatricula);
            }
        }
    }

}
