package com.student.studentsystem.service;
import com.student.studentsystem.dto.DetalleMatriculaDTO;
import com.student.studentsystem.dto.MatriculaDTO;
import com.student.studentsystem.model.DetalleMatricula;
import com.student.studentsystem.model.Matricula;

public interface IMatriculaService extends ICRUD<Matricula,Integer>{
    void saveMatricula(MatriculaDTO obj);

}
