package com.student.studentsystem.service.impl;

import com.student.studentsystem.dto.EstudianteDTO;
import com.student.studentsystem.model.Estudiante;
import com.student.studentsystem.repository.IEstudianteRepo;
import com.student.studentsystem.repository.IGenericRepo;
import com.student.studentsystem.service.IEstudianteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EstudianteServiceImpl extends CRUDImpl<Estudiante,Integer> implements IEstudianteService {

    private final IEstudianteRepo repo;

    @Override
    protected IGenericRepo<Estudiante, Integer> getRepo() {
        return repo;
    }

    @Override
    public List<Estudiante> listEstudentsOrderByDescForAge() {
        List<Estudiante> lista = repo.findAll().stream().sorted(Comparator.comparing(Estudiante::getEdad).reversed()).toList();
        return lista;
    }

    @Override
    public List<Estudiante> listCourseAndStudentRelationship() {
        return null;
    }
}
