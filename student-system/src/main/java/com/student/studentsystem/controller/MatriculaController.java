package com.student.studentsystem.controller;

import com.student.studentsystem.dto.CursoEstudianteDTO;
import com.student.studentsystem.dto.EstudianteDTO;
import com.student.studentsystem.dto.MatriculaDTO;
import com.student.studentsystem.model.DetalleMatricula;
import com.student.studentsystem.model.Estudiante;
import com.student.studentsystem.model.Matricula;
import com.student.studentsystem.service.IMatriculaService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Stream;
import static java.util.stream.Collectors.*;

@RestController
@RequestMapping("/matriculas")
@RequiredArgsConstructor
public class MatriculaController {

    private final IMatriculaService service;
    private final ModelMapper mapper;

    @PostMapping
    public ResponseEntity<MatriculaDTO> create(@Valid @RequestBody MatriculaDTO dto) throws Exception{
        service.saveMatricula(dto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/showCourseAndStudentRelationship")
    public ResponseEntity<List<CursoEstudianteDTO>> showCourseAndStudentRelationship() throws Exception{
        Stream<List<DetalleMatricula>> stream = service.readAll().stream().map(Matricula::getListaDetalleMatricula);
        Stream<DetalleMatricula> streamSaleDetail = stream.flatMap(Collection::stream);
        Map<String, List<DetalleMatricula>> byMatricula = streamSaleDetail.collect(groupingBy(d -> d.getCurso().getNombre()));
        List<CursoEstudianteDTO> listEstudianteDto = new ArrayList<>();
        for (Map.Entry<String, List<DetalleMatricula>> entry : byMatricula.entrySet()) {
            CursoEstudianteDTO cursoEstudianteDTO = new CursoEstudianteDTO();
            List<String> lisEstudiante = new ArrayList<>();
            entry.getValue().forEach((final DetalleMatricula dm) -> lisEstudiante.add(dm.getMatricula().getEstudiante().getNombres()));
            cursoEstudianteDTO.setCurso(entry.getKey());
            cursoEstudianteDTO.setEstudiantes(lisEstudiante);
            listEstudianteDto.add(cursoEstudianteDTO);
        }
        return new ResponseEntity<>(listEstudianteDto, HttpStatus.OK);
    }

    private MatriculaDTO convertToDto(Matricula obj){
        return mapper.map(obj,MatriculaDTO.class);
    }

    private Matricula convertToEntity(MatriculaDTO dto){
        return mapper.map(dto,Matricula.class);
    }

}
