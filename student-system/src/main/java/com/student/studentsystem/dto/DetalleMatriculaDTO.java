package com.student.studentsystem.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetalleMatriculaDTO {

    private Integer idDetalleMatriculaPk;
    private Integer idCurso;
    private String aula;

}
