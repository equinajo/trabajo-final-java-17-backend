package com.student.studentsystem.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstudianteDTO {

    private Integer idEstudiantePk;
    @NotNull
    @NotEmpty
    @Size(min = 3,max = 50)
    private String nombres;
    @NotNull
    @NotEmpty
    @Size(min = 3,max = 50)
    private String apellidos;
    @NotNull
    @NotEmpty
    @Size(min = 3,max = 15)
    private String dni;
    private int edad;
}
