package com.student.studentsystem.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursoDTO {

    private Integer idCursoPk;
    @NotNull
    @NotEmpty
    @Size(min = 3,max = 50)
    private String nombre;
    @NotNull
    @NotEmpty
    @Size(min = 3,max = 50)
    private String sigla;
    private boolean estado;

}
