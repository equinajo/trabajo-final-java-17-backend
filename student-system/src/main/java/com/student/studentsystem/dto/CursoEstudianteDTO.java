package com.student.studentsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursoEstudianteDTO<T> {
    private String curso;
    private List<String> estudiantes;
}
