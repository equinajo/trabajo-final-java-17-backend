package com.student.studentsystem.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatriculaDTO {

    private Integer idMatriculaPk;
    private Date fechaMatricula;
    private boolean estado;
    private EstudianteDTO estudiante;
    private Integer idEstudiante;
    private List<DetalleMatriculaDTO> listaDetalleMatricula;

}
